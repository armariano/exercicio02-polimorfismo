package br.com.itau;

public class Vip extends Ingresso{
    protected final double valorAdicional = 10;

    public Vip() {
    }

    public double retornarValorAtualizado(){
        return valor + valorAdicional;
    }
}
